
package programs;
public class CycleDetection{
    static class SinglyLinkedListNode {
        public int data;
        public SinglyLinkedListNode next;

        public SinglyLinkedListNode(int nodeData) {
            this.data = nodeData;
            this.next = null;
        }
    }

    static class SinglyLinkedList {
        public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;

        public SinglyLinkedList() {
            this.head = null;
            this.tail = null;
        }

        public void insertNode(int nodeData) {
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

            if (this.head == null) {
                this.head = node;
            } else {
                this.tail.next = node;
            }

            this.tail = node;
        }
    }
    static boolean hasCycle(SinglyLinkedListNode head) {
    SinglyLinkedListNode temp=head;
        int count=0;
        while(temp!=null)
        {
        count++;
        temp=temp.next;
        if(count>1000)
        return true;
        }
        return false;

        }
}






