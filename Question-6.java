package programs;

import java.util.Stack;
import java.util.Scanner;

public class LargestRectangle
{
    static int getMaxArea(int hist[], int n)
    {
        Stack<Integer> s = new Stack<>();

        int max = 0;
        int top;
        int tarea;

        int i = 0;
        while (i < n)
        {
            if (s.empty() || hist[s.peek()] <= hist[i])
                s.push(i++);

                // If this bar is lower than top of stack, then calculate area of rectangle
                // with stack top as the smallest (or minimum height) bar. 'i' is
                // 'right index' for the top and element before top in stack is 'left index'
            else
            {
                top = s.peek();  // store the top index
                s.pop();

                // Calculate the area with hist[top] stack as smallest bar
                tarea = hist[top] * (s.empty() ? i : i - s.peek() - 1);
                if (max < tarea)
                    max = tarea;
            }
        }

        // Now pop the remaining bars from stack and calculate area with every
        // popped bar as the smallest bar
        while (s.empty() == false)
        {
            top = s.peek();
            s.pop();
            tarea = hist[top] * (s.empty() ? i : i - s.peek() - 1);

            if (max < tarea)
                max = tarea;
        }

        return max;

    }
    public static void main(String[] args)
    {
        /*int hist[] = { 6, 2, 5, 4, 5, 1, 6 };
        System.out.println("Maximum area is " + getMaxArea(hist, hist.length));
        int hi[]={1, 2, 3, 4, 5};
        System.out.println("Maximum area is " + getMaxArea(hi, hi.length));
        */
        Scanner input=new Scanner(System.in);
        //System.out.println("enter the no. of buildings");
        int n=input.nextInt();
        int[] build=new int[n];
        for(int i=0; i<n;i++)
        {
            //System.out.println("enter the element");
            build[i]=input.nextInt();
        }
        System.out.println(/*"maximum Area is "+ */getMaxArea(build,n));
    }
}