package programs;
import java.util.Scanner;

public class ComponentsInGraph {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        UF a = new UF();
        a.init(2*n);
        for (int i = 1; i <=n; i++) {
            int g = input.nextInt();
            int b = input.nextInt();
            a.union(g, b);
        }
        int maxSize = Integer.MIN_VALUE, minSize = Integer.MAX_VALUE;
        for (int i = 1; i <= 2*n; i++) {
            int p = a.find(i);
            int s = a.size[p];
            if (s > 1) {
                if (minSize > s) minSize = s;
                if (maxSize < s) maxSize = s;
            }
        }
        System.out.println(minSize + " " + maxSize);
    }

    static class UF {
        static final int max = 30001;
        int parent[] = new int[max];
        int size[] = new int[max];

        public void init(int n) {
            for (int i = 1; i <= n; i++) {
                parent[i] = i;
                size[i] = 1;
            }
        }
        public void union(int u, int v) {
            int i = find(u);
            int j = find(v);
            //System.out.println("u= " + u + ", v= " + v + ", i= " + i + ", j= " + j);
            if (i == j) return;
            parent[i] = j;
            size[j] += size[i];
            // System.out.println("size[" + j + "]= " + size[j]);
            // if (size[j] > maxSize) maxSize = size[j];
            // if (size[j] < minSize) minSize = size[j];
        }
        public int find(int u) {
            if (parent[u] == u) return u;
            else return parent[u] = find(parent[u]);
        }
    }
}