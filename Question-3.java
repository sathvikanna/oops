package programs;


import java.util.Scanner;

public class Is_This_a_Binary_Search_Tree {
    public static boolean checkBST(BNode root) {
        boolean bool=false;
        if(root == null)
        {
            bool=true;
        }
        else if(root.left !=null)
        {
            if(root.right !=null)
            {
                if(root.left.data>root.data || root.data>root.right.data)
                {
                    bool=false;
                }
                else if(root.left.data<root.data && root.data<root.right.data)
                    bool = (checkBST(root.left) && checkBST(root.right));
            }
            else{
                if(root.left.data<root.data)
                    bool = checkBST(root.left);
                else bool = false;
            }
        }
        else if(root.right !=null)
        {
            if(root.left !=null)
            {
                if(root.left.data>root.data || root.data>root.right.data)
                {
                    bool = false;
                }
                else if(root.left.data<root.data && root.data<root.right.data)
                    bool = (checkBST(root.left) && checkBST(root.right));
            }
            else{
                if(root.right.data<root.data)
                    bool = false;
                else
                    bool = checkBST(root.right);
            }
        }
        else bool = true;
        return bool;
    }
    public static void insertE(BNode root,int data) {

        if(root == null) {
            BNode temp = new BNode(data);
            root = temp;
        }
        else if( root.data<data)
            insertE(root.right,data);
        else if(root.data>data)
            insertE(root.left,data);
        else
            System.out.println("element is already inserted");

    }
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        BNode root=null;
        for(int i=0;i<10;i++)
        {
            int k=input.nextInt();
            insertE(root,k);
        }
        boolean a=checkBST(root);
        if(a)
            System.out.println("yes");
        else
            System.out.println("No");
    }
}
class BNode{
    int data;
    BNode left;
    BNode right;
    BNode(){
        System.out.println("HI the head node is created");
    }
    BNode(int data){
        this.data=data;
        this.left=null;
        this.right=null;
    }
}

